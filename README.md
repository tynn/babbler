Babbler
=======

Falling for Spanish.
Grounded by English.

[![CircleCI](https://circleci.com/bb/tynn/babbler.svg?style=svg)](https://circleci.com/bb/tynn/babbler)


Considerations
--------------

### threading
handled in `MainActivity` - maybe a little too much

### tests
units for model and view - `MainActivity` untested

### meaningful commit history
separated by modules and features

### leaks
`babbler.recycle()` removes all references to the activity in `view`

### Kotlin vs Java
Kotlin is used for view binding and property types - saved code and annotation processing


Small project documentation
---------------------------

### how much time was invested
about 8 hours

### how was the time distributed
setup (25%) - model (15%) - view (30%) - app (25%) -- research (50%)

### decisions made to solve certain aspects of the game
`view` was considered to hold the view implementation - also a ViewHolder
now `app` only holds references to the views - using observer and callback pattern

### decisions made because of restricted time
none

### first thing to improve or add
the animation could be nicer - like a falling feather
improve theoretical stability and configuration
adding UI tests
