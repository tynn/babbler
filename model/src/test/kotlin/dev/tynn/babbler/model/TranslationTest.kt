package dev.tynn.babbler.model

import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.FunSpec

class TranslationTest : FunSpec() {

    init {
        test("Load empty translations array") {
            "[]".byteInputStream().use {
                Translation.load(it).size shouldBe 0
            }
        }

        test("Load single entry translations array") {
            "[{\"text_eng\":\"class\",\"text_spa\":\"curso\"}]".byteInputStream().use {
                Translation.load(it) shouldBe listOf(Translation("class", "curso"))
            }
        }

        test("Load multi entry translations array") {
            """[
                {
                    "text_eng" : "class",
                    "text_spa" : "curso"
                },
                {
                    "text_eng": "bell",
                    "text_spa": "timbre"
                },
                {"text_eng":"group","text_spa":"grupo"}
            ]""".byteInputStream().use {
                Translation.load(it) shouldBe listOf(Translation("class", "curso"),
                        Translation("bell", "timbre"), Translation("group", "grupo"))
            }
        }
    }
}
