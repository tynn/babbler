package dev.tynn.babbler.model

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.string
import java.io.InputStream

data class Translation(val english: String, val spanish: String) {
    companion object {
        @JvmStatic
        @Suppress("UNCHECKED_CAST")
        fun load(source: InputStream) = Parser()
                .parse(source)
                .let { it as JsonArray<JsonObject> }
                .map { Translation(it.string("text_eng")!!, it.string("text_spa")!!) }
    }
}
