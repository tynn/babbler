package dev.tynn.babbler

import android.os.Bundle
import android.support.annotation.Keep
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.View.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import dev.tynn.babbler.view.Babbler
import dev.tynn.babbler.view.Translations
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.thread

fun TextView.postText(text: String?) = post {
    setText(text)
}

class MainActivity : AppCompatActivity() {

    private val translations by lazy {
        Translations(resources.openRawResource(R.raw.words)).also {
            runOnUiThread { startAction.gone(false) }
        }
    }

    val babbler: Babbler by lazy {
        Babbler(translations) {
            it.setEnglishCount { englishCount.postText(it) }
            it.setEnglishTerm { englishTerm.postText(it) }
            it.setSpanishCount { spanishCount.postText(it) }
            it.setSpanishNote { spanishTerm.tag = it }
            it.setSpanishTerm {
                spanishTerm.postText(it)
                spanishTerm.startAnimation(topDown)
            }
        }
    }

    @get:Keep // for testing
    val topDown by lazy {
        AnimationUtils.loadAnimation(this, R.anim.top_bottom).apply {
            setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) = Unit
                override fun onAnimationStart(animation: Animation?) = Unit
                override fun onAnimationEnd(animation: Animation?) = babbler.stop()
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
                SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        SYSTEM_UI_FLAG_FULLSCREEN or
                        SYSTEM_UI_FLAG_HIDE_NAVIGATION
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        positiveAction.setOnClickListener { babbler.isMatching(true) }
        negativeAction.setOnClickListener { babbler.isMatching(false) }
        startAction.setOnClickListener {
            startAction.gone(true)
            positiveAction.gone(false)
            negativeAction.gone(false)
            babbler.start {
                runOnUiThread {
                    spanishCount.setText(R.string.label_ended)
                    spanishTerm.animation?.cancel()
                    spanishTerm.text = spanishTerm.tag?.toString()
                    positiveAction.gone(true)
                    negativeAction.gone(true)
                    startAction.gone(false)
                }
            }
        }
        thread { translations }
    }

    override fun onDestroy() {
        babbler.recycle()
        super.onDestroy()
    }

    private fun View.gone(gone: Boolean) {
        visibility = if (gone) GONE else VISIBLE
    }
}
