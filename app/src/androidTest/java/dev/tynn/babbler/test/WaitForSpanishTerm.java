package dev.tynn.babbler.test;

import android.app.Activity;
import android.support.test.espresso.IdlingResource;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static dev.tynn.babbler.R.id.negativeAction;
import static dev.tynn.babbler.R.id.positiveAction;
import static dev.tynn.babbler.R.id.spanishTerm;
import static dev.tynn.babbler.test.EspressoCompat.registerIdlingResources;

class WaitForSpanishTerm implements IdlingResource, Runnable {

    private final TextView view;
    private final Button action;
    private final boolean matching;

    private boolean isMatching;
    private ResourceCallback callback;

    private WaitForSpanishTerm(TextView view, Button action, boolean matching) {
        this.view = view;
        this.action = action;
        this.matching = matching;
    }

    @Override
    public String getName() {
        return "WaitForSpanishTerm";
    }

    @Override
    public boolean isIdleNow() {
        if (isMatching) {
            return true;
        }
        view.post(this);
        return false;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {
        this.callback = callback;
    }

    @Override
    public void run() {
        if (isMatching = view.getText().equals(view.getTag()) == matching) {
            callback.onTransitionToIdle();
            callback = null;
        } else {
            action.performClick();
            view.post(this);
        }
    }

    /**
     * Wait for {@code matching} spanish term.
     */
    static IdlingResource waitForSpanishTerm(Activity activity, boolean matching) {
        View action = activity.findViewById(matching ? negativeAction : positiveAction);
        View text = activity.findViewById(spanishTerm);
        IdlingResource res = new WaitForSpanishTerm((TextView) text, (Button) action, matching);
        registerIdlingResources(res);
        return res;
    }
}
