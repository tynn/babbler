package dev.tynn.babbler.test;

import android.app.Activity;
import android.widget.TextView;

import dev.tynn.babbler.view.Babbler.Observer;

import static dev.tynn.babbler.MainActivityKt.postText;

class SetText implements Observer {

    private final TextView view;

    private SetText(TextView view) {
        this.view = view;
    }

    @Override
    public void updated(String text) {
        postText(view, text);
    }

    static Observer setText(Activity activity, int id) {
        return new SetText((TextView) activity.findViewById(id));
    }
}
