package dev.tynn.babbler.test;

import android.support.test.espresso.matcher.BoundedMatcher;
import android.view.View;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

class IsTextTag extends BoundedMatcher<View, TextView> {

    private IsTextTag() {
        super(TextView.class);
    }

    @Override
    protected boolean matchesSafely(TextView view) {
        return view.getText().equals(view.getTag());
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("with string from tag");
    }

    static Matcher<View> withTagText() {
        return new IsTextTag();
    }
}
