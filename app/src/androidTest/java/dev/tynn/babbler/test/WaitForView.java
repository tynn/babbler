package dev.tynn.babbler.test;

import android.app.Activity;
import android.graphics.Rect;
import android.support.test.espresso.IdlingResource;
import android.view.View;

import static dev.tynn.babbler.test.EspressoCompat.registerIdlingResources;

class WaitForView implements IdlingResource {

    private final View view;
    private final Rect rect;

    private boolean isVisible;
    private ResourceCallback callback;

    private WaitForView(View view) {
        this.view = view;
        this.rect = new Rect();
    }

    @Override
    public String getName() {
        return "WaitForView";
    }

    @Override
    public boolean isIdleNow() {
        if (isVisible) {
            return true;
        }
        if (view.isShown()) {
            if (view.getGlobalVisibleRect(rect)) {
                callback.onTransitionToIdle();
                callback = null;
                return isVisible = true;
            }
            // somehow view is visible but not displayed - layout again
            view.requestLayout();
        }
        return false;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {
        this.callback = callback;
    }

    /**
     * Wait for {@code view} to be visible for the first time.
     */
    static IdlingResource waitForView(Activity activity, int id) {
        IdlingResource res = new WaitForView(activity.findViewById(id));
        registerIdlingResources(res);
        return res;
    }
}
