package dev.tynn.babbler.test;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;

import java.util.Collection;

class EspressoCompat {

    static boolean registerIdlingResources(IdlingResource... resources) {
        return IdlingRegistry.getInstance().register(resources);
    }

    static boolean unregisterIdlingResources(IdlingResource... resources) {
        return IdlingRegistry.getInstance().unregister(resources);
    }

    static Collection<IdlingResource> getIdlingResources() {
        return IdlingRegistry.getInstance().getResources();
    }
}
