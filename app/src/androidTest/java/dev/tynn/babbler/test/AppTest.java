package dev.tynn.babbler.test;

import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.lang.reflect.Field;

import dev.tynn.babbler.MainActivity;
import dev.tynn.babbler.view.Babbler;
import dev.tynn.babbler.view.Observers;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withTagValue;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static dev.tynn.babbler.R.id.englishCount;
import static dev.tynn.babbler.R.id.englishTerm;
import static dev.tynn.babbler.R.id.negativeAction;
import static dev.tynn.babbler.R.id.positiveAction;
import static dev.tynn.babbler.R.id.spanishCount;
import static dev.tynn.babbler.R.id.spanishTerm;
import static dev.tynn.babbler.R.id.startAction;
import static dev.tynn.babbler.R.string.app_name;
import static dev.tynn.babbler.test.EspressoCompat.getIdlingResources;
import static dev.tynn.babbler.test.EspressoCompat.unregisterIdlingResources;
import static dev.tynn.babbler.test.IsTextTag.withTagText;
import static dev.tynn.babbler.test.SetText.setText;
import static dev.tynn.babbler.test.WaitForSpanishTerm.waitForSpanishTerm;
import static dev.tynn.babbler.test.WaitForView.waitForView;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

@SuppressWarnings("WeakerAccess")
public class AppTest {

    final static IdlingResource[] I = {null};

    @Rule
    public final ActivityTestRule<MainActivity> app
            = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        observers().setSpanishTerm(setText(app.getActivity(), spanishTerm));
        waitForView(app.getActivity(), startAction);
    }

    @After
    public void clearIdlingResources() {
        unregisterIdlingResources(getIdlingResources().toArray(I));
    }

    @Test
    public void inactiveState_on_startup() throws Exception {
        onView(withId(startAction))
                .check(matches(isDisplayed()));
        onView(withId(positiveAction))
                .check(matches(not(isDisplayed())));
        onView(withId(negativeAction))
                .check(matches(not(isDisplayed())));
        onView(withId(englishCount))
                .check(matches(withText("")));
        onView(withId(englishTerm))
                .check(matches(withText(app_name)));
        onView(withId(spanishCount))
                .check(matches(withText("")));
        onView(withId(spanishTerm))
                .check(matches(withText(app_name)));
        onView(withId(spanishTerm))
                .check(matches(withTagValue(nullValue())));
    }

    @Test
    public void inactiveState_on_timeout() throws Exception {
        onView(withId(startAction))
                .perform(click())
                .check(matches(not(isDisplayed())));
        animationListener().onAnimationEnd(null);
        onView(withId(spanishTerm))
                .check(matches(withTagText()));
        onView(withId(englishTerm))
                .check(matches(not(withText(app_name))));
        onView(withId(startAction))
                .check(matches(isDisplayed()));
        onView(withId(positiveAction))
                .check(matches(not(isDisplayed())));
        onView(withId(negativeAction))
                .check(matches(not(isDisplayed())));
    }

    @Test
    public void clickStartAction() throws Exception {
        onView(withId(startAction))
                .perform(click())
                .check(matches(not(isDisplayed())));
        onView(withId(positiveAction))
                .check(matches(isDisplayed()));
        onView(withId(negativeAction))
                .check(matches(isDisplayed()));
    }

    @Test
    public void clickPositiveButton_match_true() throws Exception {
        onView(withId(startAction))
                .perform(click())
                .check(matches(not(isDisplayed())));
        waitForSpanishTerm(app.getActivity(), true);
        onView(withId(positiveAction))
                .perform(click());
        onView(withId(englishCount))
                .check(matches(withText("1")));
        onView(withId(startAction))
                .check(matches(not(isDisplayed())));
        onView(withId(positiveAction))
                .check(matches(isDisplayed()));
        onView(withId(negativeAction))
                .check(matches(isDisplayed()));
    }

    @Test
    public void clickNegativeButton_match_false() throws Exception {
        onView(withId(startAction))
                .perform(click())
                .check(matches(not(isDisplayed())));
        waitForSpanishTerm(app.getActivity(), false);
        onView(withId(negativeAction))
                .perform(click());
        onView(withId(spanishCount))
                .check(matches(withText("1")));
        onView(withId(startAction))
                .check(matches(not(isDisplayed())));
        onView(withId(positiveAction))
                .check(matches(isDisplayed()));
        onView(withId(negativeAction))
                .check(matches(isDisplayed()));
    }

    @Test
    public void clickNegativeButton_match_true() throws Exception {
        onView(withId(startAction))
                .perform(click())
                .check(matches(not(isDisplayed())));
        waitForSpanishTerm(app.getActivity(), true);
        onView(withId(negativeAction))
                .perform(click());
        onView(withId(spanishTerm))
                .check(matches(withTagText()));
        onView(withId(spanishCount))
                .check(matches(withText(dev.tynn.babbler.R.string.label_ended)));
        onView(withId(startAction))
                .check(matches(isDisplayed()));
        onView(withId(positiveAction))
                .check(matches(not(isDisplayed())));
        onView(withId(negativeAction))
                .check(matches(not(isDisplayed())));
    }


    AnimationListener animationListener() {
        return (AnimationListener) fieldValue("mListener", Animation.class, app.getActivity().getTopDown());
    }

    Observers observers() {
        return (Observers) fieldValue("observers", Babbler.class, app.getActivity().getBabbler());
    }

    static Object fieldValue(String fieldName, Class<?> type, Object object) {
        try {
            Field field = type.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
