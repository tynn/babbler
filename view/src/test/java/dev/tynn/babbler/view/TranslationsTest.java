package dev.tynn.babbler.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.InputStream;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TranslationsTest {

    Translations translations;

    @Before
    public void setup() throws Exception {
        InputStream source = getClass().getClassLoader().getResourceAsStream("words.json");
        translations = new Translations(source);
    }

    @Test
    public void isCurrentSpanishTerm() throws Exception {
        translations.nextEnglishTerm();

        assertThat(translations.isCurrentSpanishTerm(translations.currentSpanishTerm), is(true));
        assertThat(translations.isCurrentSpanishTerm("AnyTerm"), is(false));
    }

    @Test
    public void nextEnglishTerm() throws Exception {
        translations.nextIndex = translations.translations.size() / 2;
        String expected = translations.translations.get(translations.nextIndex).getEnglish();

        String actual = translations.nextEnglishTerm();

        assertThat(actual, is(expected));
    }

    @Test
    public void randomSpanishTerm() throws Exception {
        translations.nextEnglishTerm();
        ArrayList<String> spanishTerms = new ArrayList<>();

        for (int i = 0; i < 17; i++) {
            spanishTerms.add(translations.randomSpanishTerm());
        }

        assertThat(spanishTerms, hasItem(translations.currentSpanishTerm));
        while (spanishTerms.remove(translations.currentSpanishTerm)) ;
        assertThat(spanishTerms.size(), not(0));
    }
}
