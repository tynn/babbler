package dev.tynn.babbler.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class BabblerTest {

    final Observers.Callback observersCallback = new Observers.Callback() {
        @Override
        public void bind(Observers observers) {
            observers.setEnglishCount(observer1);
            observers.setEnglishTerm(observer1);
            observers.setSpanishCount(observer2);
            observers.setSpanishNote(observer1);
            observers.setSpanishTerm(observer2);
        }
    };

    Babbler babbler;

    @Mock
    Babbler.Observer observer1;
    @Mock
    Babbler.Observer observer2;
    @Mock
    Translations translations;
    @Mock
    Babbler.Callback callback;

    @Before
    public void setup() {
        babbler = new Babbler(translations, observersCallback);
    }

    @Test
    public void start() throws Exception {
        doReturn("EngTerm").when(translations).nextEnglishTerm();
        doReturn("SpaTerm").when(translations).randomSpanishTerm();
        translations.currentSpanishTerm = "SpaTerm";

        babbler.start(callback);

        verify(observer1).updated("0");
        verify(observer1).updated("EngTerm");
        verify(observer1).updated("SpaTerm");
        verify(translations).reset();
        verify(observer2).updated("0");
        verify(observer2).updated("SpaTerm");
    }

    @Test
    public void stop() throws Exception {
        babbler.callback = callback;

        babbler.stop();

        verify(callback).ended();
    }

    @Test
    public void recycle() throws Exception {
        babbler.callback = callback;

        babbler.recycle();

        assertThat(babbler.observers.englishCount, is(Observers.N));
        assertThat(babbler.observers.englishTerm, is(Observers.N));
        assertThat(babbler.observers.spanishCount, is(Observers.N));
        assertThat(babbler.observers.spanishNote, is(Observers.N));
        assertThat(babbler.observers.spanishTerm, is(Observers.N));
        assertThat(babbler.callback, nullValue());
    }

    @Test
    public void isMatching_error() throws Exception {
        doReturn(true).when(translations).isCurrentSpanishTerm("SpaTerm");
        babbler.callback = callback;
        babbler.spanishTerm = "SpaTerm";

        babbler.isMatching(false);

        verify(callback).ended();
    }

    @Test
    public void isMatching_false() throws Exception {
        doReturn(false).when(translations).isCurrentSpanishTerm("SpaTerm");
        doReturn("SpaTerm").when(translations).randomSpanishTerm();
        babbler.callback = callback;
        babbler.spanishTerm = "SpaTerm";
        babbler.spanishCount = 5;

        babbler.isMatching(false);

        verify(observer2).updated("6");
        verify(observer2).updated("SpaTerm");
    }

    @Test
    public void isMatching_true() throws Exception {
        doReturn(true).when(translations).isCurrentSpanishTerm("SpaTerm");
        doReturn("EngTerm").when(translations).nextEnglishTerm();
        doReturn("SpaTerm").when(translations).randomSpanishTerm();
        babbler.callback = callback;
        babbler.spanishCount = 2;
        babbler.spanishTerm = "SpaTerm";
        translations.nextIndex = 3;

        babbler.isMatching(true);

        verify(observer1).updated("3");
        verify(observer1).updated("EngTerm");
        verify(observer2).updated("0");
        verify(observer2).updated("SpaTerm");
    }
}
