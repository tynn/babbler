package dev.tynn.babbler.view;

public class Observers {

    Babbler.Observer englishCount = N;
    Babbler.Observer englishTerm = N;
    Babbler.Observer spanishCount = N;
    Babbler.Observer spanishNote = N;
    Babbler.Observer spanishTerm = N;

    Observers() {
    }

    public void setEnglishCount(Babbler.Observer observer) {
        englishCount = observer == null ? N : observer;
    }

    public void setEnglishTerm(Babbler.Observer observer) {
        englishTerm = observer == null ? N : observer;
    }

    public void setSpanishCount(Babbler.Observer observer) {
        spanishCount = observer == null ? N : observer;
    }

    public void setSpanishNote(Babbler.Observer observer) {
        spanishNote = observer == null ? N : observer;
    }

    public void setSpanishTerm(Babbler.Observer observer) {
        spanishTerm = observer == null ? N : observer;
    }

    static final Babbler.Observer N = new Babbler.Observer() {
        @Override
        public void updated(String text) {
        }
    };

    public interface Callback {
        void bind(Observers observers);
    }
}
