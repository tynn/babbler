package dev.tynn.babbler.view;

import android.support.annotation.Keep;

public class Babbler {

    final Translations translations;
    @Keep // for test reflections
    final Observers observers;

    Callback callback;
    int spanishCount;
    String spanishTerm;

    public Babbler(Translations translations, Observers.Callback callback) {
        this.translations = translations;
        this.observers = new Observers();
        callback.bind(this.observers);
    }

    public void start(Callback callback) {
        this.callback = callback;
        observers.englishCount.updated("0");
        translations.reset();
        nextEnglishTerm();
    }

    public void stop() {
        callback.ended();
    }

    public void recycle() {
        callback = null;
        observers.setEnglishCount(null);
        observers.setEnglishTerm(null);
        observers.setSpanishCount(null);
        observers.setSpanishNote(null);
        observers.setSpanishTerm(null);
        translations.reset();
    }

    void nextEnglishTerm() {
        try {
            spanishCount = 0;
            observers.spanishCount.updated("0");
            observers.englishTerm.updated(translations.nextEnglishTerm());
            observers.spanishNote.updated(translations.currentSpanishTerm);
            nextSpanishTerm();
        } catch (IndexOutOfBoundsException e) {
            callback.ended();
        }
    }

    void nextSpanishTerm() {
        observers.spanishTerm.updated(spanishTerm = translations.randomSpanishTerm());
    }

    public void isMatching(boolean matching) {
        boolean currentMatch = translations.isCurrentSpanishTerm(spanishTerm);
        if (currentMatch != matching) {
            callback.ended();
        } else if (currentMatch) {
            observers.englishCount.updated(String.valueOf(translations.nextIndex));
            nextEnglishTerm();
        } else {
            observers.spanishCount.updated(String.valueOf(++spanishCount));
            nextSpanishTerm();
        }
    }

    public interface Callback {
        void ended();
    }

    public interface Observer {
        void updated(String text);
    }
}
