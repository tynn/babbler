package dev.tynn.babbler.view;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import dev.tynn.babbler.model.Translation;

public class Translations extends Random {

    final ArrayList<Translation> translations = new ArrayList<>();

    String currentSpanishTerm;
    int nextIndex;

    public Translations(InputStream source) throws IOException {
        translations.addAll(Translation.load(source));
    }

    void reset() {
        nextIndex = 0;
        Collections.shuffle(translations);
    }

    boolean isCurrentSpanishTerm(String spanishTerm) {
        return spanishTerm.equals(currentSpanishTerm);
    }

    String nextEnglishTerm() {
        Translation translation = translations.get(nextIndex++);
        currentSpanishTerm = translation.getSpanish();
        return translation.getEnglish();
    }

    String randomSpanishTerm() {
        // 2 bits = 1 in 4
        if (next(2) == 0) {
            return currentSpanishTerm;
        }
        return translations.get(nextInt(translations.size())).getSpanish();
    }
}
